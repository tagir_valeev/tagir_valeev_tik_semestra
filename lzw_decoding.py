def decode_lzw(compressed_data):
    next_code = 256
    decoded_data = ""
    string = ""

    # Создание и инициализация словаря
    dict_size = 256
    dict_ = dict([(x, chr(x)) for x in range(dict_size)])

    # перебор кодов
    # Алгоритм LZW
    for code in compressed_data:
        if not (code in dict_):
            dict_[code] = string + (string[0])
        decoded_data += dict_[code]
        if not (len(string) == 0):
            dict_[next_code] = string + (dict_[code][0])
            next_code += 1
        string = dict_[code]

    return decoded_data


if __name__ == '__main__':
    file_name = str(input('Введите имя файла: '))
    encoded = []

    with open(file_name, 'r') as f:
        while chars := f.read(8):
            print(chars)
            print(int(chars, 2))
            encoded.append(int(chars, 2))

    decompressed = decode_lzw(encoded)
    with open("lzw_decoded.txt", 'w') as f:
        f.write(decompressed)
        f.close()
    print(decompressed)